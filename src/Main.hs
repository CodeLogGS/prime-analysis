import Data.List
import System.IO
import Data.Char hiding (length, init, head)
import Tools

prompt :: String -> IO String
prompt text = do
  putStr text
  hFlush stdout
  getLine

main = do
  line <- prompt "Enter a maximum value (10^n): "
  debug <- prompt "Show debug values? (Y/n): "

  -- Empty line for better-looking output.
  putStrLn ""

  let maxVal = 10 ^ (read line :: Int)
  let primes = [x | x <- [1..maxVal], isPrime x 2]
  let spacing = init $ getSpacing primes

  let bDebug = toUpper (head debug) == 'Y'

  if bDebug then do
    putStrLn $ "Primes: " ++ show primes
    putStrLn $ "Spacing: " ++ show spacing
    putStrLn $ "Ordered spacing: " ++ show (sort spacing)
    putStrLn $ "Average Spacing: " ++ show (average spacing)

  else do
    let c = genPConst (concatMap show spacing)
    let tNums = [c ^ 3 ^ n | n <- [1..20]]
    let d = [isPrime (floor x) 2 | x <- tNums]
    putStrLn $ "Average Spacing: " ++ show (average spacing)
    putStrLn $ "Derived constant: " ++ show c

    if bDebug then do
      putStrLn $ "Attempt nums: " ++ show tNums
      putStrLn $ "List of attempts: " ++ show d
    else
      putStrLn ""
    if (False `elem` d) && (True `elem` d) then
      putStrLn $ "True for n <= " ++ show (length (takeWhile (/= False) d))

    else if False `elem` d then
      putStrLn $ "Not true for any n in [1.." ++ show maxVal ++ "]"

    else
      putStrLn "True for all!"
