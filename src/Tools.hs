module Tools (isPrime, getSpacing, average, genPConst) where

import Data.List

isPrime :: Int -> Int -> Bool
isPrime n i
  | n == 2 = True
  | n < 2 = False
  | n `mod` i == 0 = False
  | i * i > n = True
  | otherwise = isPrime n (i + 1)

getSpacing :: [Int] -> [Int]
getSpacing [a] = [0]
getSpacing (x:xs) = (head xs - x) : getSpacing xs

average :: [Int] -> Float
average n = fromIntegral (sum n) / fromIntegral (length n)

genPConst :: String -> Double
genPConst [] = error "Empty list."
genPConst (x:xs) =
  let l = read [x] :: Int
      b = (read ("0." ++ xs) :: Double)
  in
      fromIntegral l + b
