# Prime Analysis

This is a program for analysing the spacing between primes.

## Building

This project uses the Cabal build system.

1. First make sure you have Cabal installed by running: `# pacman -S cabal-install` on Arch Linux.
2. In the root directory of the project run: `$ cabal configure` followed by `$ cabal build` to build the project and `$ cabal run` to run the built executable.
