# Revision history for Prime-Analysis

## 0.0.1 -- 2018-06-11

* First version. Released on an unsuspecting world.

## 1.0.1 -- 2018-06-12

* Added some printing functions for analysing the spacing between primes.
* Created functions for testing wether a number is a prime.
* Tagged this release as v1.0.1

## 1.0.2 -- 2018-06-12

* Changed the way the program determines the information to output.

## 1.1.0-rc1 --2018-06-12

* Added functionality for testing mil's conjecture.

## 1.1.0 -- 2018-06-12

* Edited main.hs for final release 1.1.0
* Added some functionality.

## 1.1.1 -- 2018-06-13

* Moved all utility functions to module: 'Tools'
